﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cockroach : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float waitingTime;

    [SerializeField] Transform upperLeftPoint;
    [SerializeField] Transform lowerRightPoint;

    float remainingWaitingTime;
    int randomDestinationPoint;

    float randomX;
    float randomY;
    Vector2 newPoint;
    Vector3 point;

    void Start()
    {
        remainingWaitingTime = waitingTime;
        FindNewPoint();
    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, newPoint, speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, newPoint) < 0.2f)
        {
            if (remainingWaitingTime <= 0)
            {
                FindNewPoint();
                remainingWaitingTime = waitingTime;
            }
            else
            {
                remainingWaitingTime -= Time.deltaTime;
            }
        }
        else
        {
            transform.rotation = Quaternion.LookRotation(Vector3.forward, point - transform.position);
        }
    }

    void FindNewPoint()
    {
        randomX = Random.Range(upperLeftPoint.position.x, lowerRightPoint.position.x);
        randomY = Random.Range(upperLeftPoint.position.y, lowerRightPoint.position.y);

        newPoint = new Vector2(randomX, randomY);
        point = new Vector3(randomX, randomY, 0);
    }
}
